/*
 * ------------------------------------------------------------------------
 *  MULTI-HIT ADC AND TDC or QDC (depending on firmware)
 * ------------------------------------------------------------------------
 * 32 SIGNAL INPUTS ARE PRESENT ON THE VME MODULE
 *
 * SCP FIRMWARE:
 * 34 CHANNELS ARE ENCODED
 * data [0..31] : ADC VALUE FOR INPUT 0..31
 * data [32..63]: TDC VALUE FOR INPUT 0..31
 * data [64]    : TDC VALUE FOR TRIGGER INPUT T0
 * data [65]    : TDC VALUE FOR TRIGGER INPUT T1
 *
 * QDC FIRMWARE:
 * 48 CHANNELS ARE ENCODED
 * data [0..31] : ADC VALUE LONG INTEGRATION
 * data [32..63]: TDC VALUE FOR INPUT 0..31
 * data [64]    : TDC VALUE FOR TRIGGER INPUT T0
 * data [65]    : TDC VALUE FOR TRIGGER INPUT T1
 * data [96..127]: ADC VALUE SHORT INTEGRATION
 */

VME_MESYTEC_MDPP32(geom)
{
  MEMBER(DATA16_OVERFLOW adc[32] ZERO_SUPPRESS_MULTI(20));
  MEMBER(DATA16_OVERFLOW tdc[32] ZERO_SUPPRESS_MULTI(20));
  MEMBER(DATA16_OVERFLOW trig_tdc[32] ZERO_SUPPRESS_MULTI(20));
  MEMBER(DATA16_OVERFLOW adc_short[32] ZERO_SUPPRESS_MULTI(20));

  MARK_COUNT(start);
  UINT32 header NOENCODE
  {
    0_9:   word_number; // includes end_of_event
    10_12: adc_res;
    13_15: tdc_res;
    16_23: geom = MATCH(geom);
    24_29: 0b000000;
    30_31: 0b01;
  }

  several UINT32 ch_data NOENCODE
  {
    0_15:  value;
    16_20: channel;
    21_22: ch_kind;
    23: overflow;
    24: pileup;
    25_27: 0b000;
    28_31: 0b0001;

    if (ch_kind == 0) {
      ENCODE(adc[channel],
	     (value = value, overflow = overflow, pileup = pileup));
    }
    if (ch_kind == 1) {
      ENCODE(tdc[channel],
	     (value = value, overflow = overflow, pileup = pileup));
    }
    if (ch_kind == 2) {
      ENCODE(trig_tdc[channel],
	     (value = value, overflow = overflow, pileup = pileup));
    }
    if (ch_kind == 3) {
      ENCODE(adc_short[channel],
	     (value = value, overflow = overflow, pileup = pileup));
    }
  }

  several UINT32 fill_word NOENCODE
  {
    0_31: 0x0;
  }

  UINT32 end_of_event
  {
    0_29:  counter;
    30_31: 0b11;
  }

  MARK_COUNT(end);
  CHECK_COUNT(header.word_number,start,end,-4,4);
}

VME_MESYTEC_MDPP32_FREE(geom)
{
  MEMBER(DATA16_OVERFLOW adc[32] ZERO_SUPPRESS_MULTI(20000));
  MEMBER(DATA16_OVERFLOW trig[2] ZERO_SUPPRESS_MULTI(20000));
  MEMBER(DATA32 time[32] ZERO_SUPPRESS_MULTI(20000));
  MEMBER(DATA32 trigt[2] ZERO_SUPPRESS_MULTI(20000));

  UINT32 data NOENCODE
  {
    0_15: amp;
    16: ov;
    17: pu;
    18_22: ch;
    23: is_trig;
    24_29: geom = MATCH(geom);
    30_31: 0b01;

    if (!is_trig) {
      ENCODE(adc[ch], (value = amp, overflow = ov));
    }
    if (is_trig) {
      ENCODE(trig[ch], (value = amp, overflow = ov));
    }
  }

  UINT32 end_of_event NOENCODE
  {
    0_29:  time;
    30_31: 0b11;
  }

  if (!data.is_trig) {
    ENCODE(time[data.ch], (value = end_of_event.time));
  }
  if (data.is_trig) {
    ENCODE(trigt[data.ch], (value = end_of_event.time));
  }
}
