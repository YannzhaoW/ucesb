// -*- C++ -*-

/* This file is part of UCESB - a tool for data unpacking and processing.
 *
 * Copyright (C) 2016  GSI Helmholtzzentrum fuer Schwerionenforschung GmbH
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA  02110-1301  USA
 */

/* This file provides specifications for:
 *
 * VME_CAEN_V767A  - 64 ch
 * VME_CAEN_V767  - 128 ch
 */

/* Note: regarding unstable geom header/footer field:
 *
 * Define VME_CAEN_V767x_NO_GEOM_MATCH before including this file (or
 * spec/spec.spec) if the geom header/footer field is not fixed but
 * varies, e.g. between experiments.  Note: the geom field can then
 * not be used to separate data between several modules.
 */

#ifndef VME_CAEN_V767x

# define VME_CAEN_V767x_NUM_CH  64
# define VME_CAEN_V767x    VME_CAEN_V767A
# include "vme_caen_v767.spec"
# undef  VME_CAEN_V767x_NUM_CH
# undef  VME_CAEN_V767x

# define VME_CAEN_V767x_NUM_CH  128
# define VME_CAEN_V767x    VME_CAEN_V767
# include "vme_caen_v767.spec"
# undef  VME_CAEN_V767x_NUM_CH
# undef  VME_CAEN_V767x

#else

VME_CAEN_V767x(geom)
{
  // Should have our own data type...?
  MEMBER(DATA24 data[VME_CAEN_V767x_NUM_CH] ZERO_SUPPRESS_MULTI(128));

  UINT32 header
    {
      0_11:  event_number;
      12_20: undef1;
      21:    0;
      22:    1;
      23_26: undef2;
# ifndef VME_CAEN_V767x_NO_GEOM_MATCH
      27_31: geom = MATCH(geom);
# else
      27_31: geom;
# endif
    }

  MARK_COUNT(start_data);

  several UINT32 ch_data NOENCODE
    {
      0_19:  value;
      20:    edge;
      21:    0;
      22:    0;
      23:    start;
# if VME_CAEN_V767x_NUM_CH == 128
      24_30: channel;
# endif
# if VME_CAEN_V767x_NUM_CH == 64
      24_29: channel;
      30:    0;
# endif
      31:    undefined;

      ENCODE(data[channel], (value=value));
    }

  MARK_COUNT(end_data);

  UINT32 eob
    {
      0_15:  number_of_hits;
      16_20: undef1;
      21:    1;
      22:    0;
      23:    undef2;
      24_26: status;
# ifndef VME_CAEN_V767x_NO_GEOM_MATCH
      27_31: geom = CHECK(geom);
# else
      27_31: geom;
# endif
    }

  CHECK_COUNT(eob.number_of_hits, start_data, end_data,0,4);
}

#endif
